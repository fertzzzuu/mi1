import java.util.ArrayList;
import java.util.List;

public class Package {

    public int width;
    public int height;
    public List<Item> items;
    public Matrix matrix;

    public Package(int h, int w){
        items = new ArrayList<>();
        width = w;
        height = h;
        matrix = Matrix.zeros(width, height);
    }

    public void makeOrder() {
        items.sort(new ItemAreaComparator());
    }

}
