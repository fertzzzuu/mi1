import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        String[] dimensions = line.split("\\t");
        Package pack = new Package(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]));

        int numOfItems = Integer.parseInt(scanner.nextLine()); //ignore num of items

        for (int i= 0; i < numOfItems; i++){
            line = scanner.nextLine();
            String[] itemDimensions = line.split("\\t"); //todo: tab
            Item item = new Item(Integer.parseInt(itemDimensions[0]), Integer.parseInt(itemDimensions[1]));
            item.history = Matrix.zeros(pack.width, pack.height);
            item.number = Matrix.itemNumber++;
            pack.items.add(item);
        }

       pack.makeOrder();

        for (Item i :
                pack.items) {
            pack.matrix.put(i);
        }

        pack.matrix.print();


    }
}
