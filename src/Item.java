public class Item {
    public int height;
    public int width;
    public int number;
    public Matrix history;

    public Item(int h, int w) {
        height = h;
        width = w;
    }

    public void rotate() {
        int t = height;
        height = width;
        width = t;
    }

    public int square() {
        return height * width;
    }




}

