import java.util.Comparator;

public class ItemAreaComparator implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        return o2.square() - o1.square();
    }
}