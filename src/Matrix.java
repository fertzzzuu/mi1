public class Matrix {
    public static int itemNumber = 1;
    public int[][] data;
    private int w;
    private int h;

    public Matrix(int w, int h) {
        data = new int[w][h];
        this.w = w;
        this.h = h;

    }

    public Matrix(int[][] d, int w, int h) {
        this(w, h);
        data = d;
    }

    public static Matrix zeros(int w, int h) {
        int[][] t = new int[w][h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                t[i][j] = 0;
            }
        }
        return new Matrix(t, w, h);
    }

    public void print() {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                System.out.print(data[j][i]);
                if (j != w - 1)
                    System.out.print("\t");
            }
            System.out.print("\n");
        }
    }

    public void put(Item item) {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                if (!isOccupied(j, i)) {
                    if (fits(j, i, item.width, item.height)) {
                        putItem(item, i, j);
                        return;
                    } else {
                        item.rotate();
                        if (fits(j, i, item.width, item.height)) {
                            putItem(item, i, j);
                            return;
                        }
                    }
                }
            }
        }
    }

    private void putItem(Item item, int i, int j) {
        for (int k = i; k < i + item.height; k++) {
            for (int l = j; l < j + item.width; l++) {
                data[l][k] = item.number;
                item.history.data[l][k] = 1;
            }
        }
    }

    public boolean isOccupied(int x, int y) {
        return data[x][y] != 0;
    }

    private boolean fits(int x, int y, int w, int h) {
        if ((x + w) <= (this.w) && (y + h) <= (this.h)) {
            for (int i = x; i < x + w; i++) {
                for (int j = y; j < y + h; j++) {
                    if (data[i][j] != 0) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    private boolean beenThere(int x, int y, Item item){
        for (int i = x; i < x + item.width; i++) {
            for (int j = y; j < y + item.height; j++) {
                if (item.history.data[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void deleteItem(int itemNumber){
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if(data[i][j] == itemNumber){
                    data[i][j] = 0;
                }
            }
        }
        Matrix.itemNumber--;
    }


}
